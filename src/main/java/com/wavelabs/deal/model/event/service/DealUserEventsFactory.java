package com.wavelabs.deal.model.event.service;

import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wavelabs.deal.domain.model.deal.Deal;
import com.wavelabs.deal.domain.repositories.DealRepository;
import com.wavelabs.deal.domain.repositories.UserRepository;
import com.wavelabs.deal.model.events.DealUserEvents;
import com.wavelabs.deal.model.events.EventType;

/**
 * 
 * @author thejasreem
 * DealUserEventsFactory() method is for setting events when
 *         the user performs actions on the domain.
 */

@Component
public class DealUserEventsFactory {
	@Autowired
	DealRepository dealRepo;
	@Autowired
	UserRepository userRepo;
	public DealUserEvents dealUserEventsForInterested(EventType type, Integer dealUuid, Integer userid) {
		DealUserEvents event = new DealUserEvents();
		Deal deal = dealRepo.findById(dealUuid);
		event.setDeal(deal);
		//User user = userRepo.findById(userId);
		//event.setUser(user);
		event.setTimeStamp(Calendar.getInstance());
		event.setEventType(EventType.Interested);
		return event;

	}
	public DealUserEvents dealUserEventsForViewed(EventType type, Integer dealUuid, String userId) {
		DealUserEvents event = new DealUserEvents();
		event.setTimeStamp(Calendar.getInstance());
		Deal deal = dealRepo.findById(dealUuid);
		event.setDeal(deal);
		//User user = userRepo.findById(userId);
		//event.setUser(user);
		event.setEventType(EventType.Viewed);
		return event;

	}
	public DealUserEvents dealUserEventsForInvested(EventType type, Integer dealUuid, String userId) {
		DealUserEvents event = new DealUserEvents();
		event.setTimeStamp(Calendar.getInstance());
		Deal deal = dealRepo.findById(dealUuid);
		event.setDeal(deal);
		//User user = userRepo.findById(userId);
		//event.setUser(user);
		event.setEventType(EventType.Invested);
		return event;

	}
	
}
