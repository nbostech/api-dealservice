package com.wavelabs.deal.model.event.service;

import com.wavelabs.deal.model.events.DealUserEvents;

/**
 * 
 * @author thejasreem ShowInterestEventService is an interface.
 *         createEventForShowInterest() method call for creating an event for
 *         showing interest on particular deal.
 */
public interface DealUserEventsService {
	boolean createEventsForDeals(DealUserEvents dealUserEvents);
}
