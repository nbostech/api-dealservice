package com.wavelabs.deal.model.utils;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
@Component
public class DealDetailsOfContentService {
	@Autowired
	static Environment env;

	String contentDealObject = "";
	static JSONParser jParser = new JSONParser();
	static JSONObject jObject = null;
	
	@SuppressWarnings("rawtypes")
	public static  String jsonParseObject(ResponseEntity response){
		String jsonString = (String) response.getBody();
		System.out.println("jSonString - "+jsonString);
		// parsing data
		try {
			jObject = (JSONObject) (jParser.parse(jsonString));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		//String dUuid = (String) jObject.get("id");
		String dealName = (String) jObject.get("company");
		//System.out.println("duuid:" + dUuid);
		System.out.println("dealName-"+dealName);
		return dealName;
	}

}
