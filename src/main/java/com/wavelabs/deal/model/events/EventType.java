package com.wavelabs.deal.model.events;
/**
 * 
 * @author thejasreem
 * EventType is an enum.
 * It is used to define events.
 * It has two event types like Viewed, Interested, Invested..... 
 */
public enum EventType {
	Viewed,
	Interested,
	Invested
}
