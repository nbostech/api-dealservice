package com.wavelabs.deal.model.services.impl;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.wavelabs.deal.domain.model.deal.Deal;
import com.wavelabs.deal.domain.model.investment.Investment;
import com.wavelabs.deal.domain.model.user.User;
import com.wavelabs.deal.domain.repositories.DealRepository;
import com.wavelabs.deal.domain.repositories.InvestmentRepository;
import com.wavelabs.deal.domain.repositories.UserRepository;
import com.wavelabs.deal.model.service.InvestmentService;
import com.wavelabs.deal.model.utils.DealDetailsOfContentService;

@Service
public class InvestmentServiceImpl implements InvestmentService {

	static Logger log = Logger.getLogger(InvestmentServiceImpl.class);
	@Autowired
	InvestmentRepository investRepo;
	@Autowired
	DealRepository dealRepo;
	@Autowired
	UserRepository userRepo;
	@Autowired
	Environment env;
	Investment investment = new Investment();
	/*
	 * processInvestAmount() method is for adding Amount based on deals. Store
	 * the amount, dealId, userId on Investment object. Get the total amount by
	 * using dealId. Update the securedFunding with that total amount which is
	 * in Deal Object.
	 */

	public Investment processInvestAmount(String dealUuid, String userId, double amount) {
		@SuppressWarnings("rawtypes")
		ResponseEntity response = callDealByIdOfContentService(dealUuid);
		System.out.println(dealUuid);
		String dealName = jsonParseObject(response);
		System.out.println("dealName-" + dealName);
		Deal deal = dealRepo.findByUuid(dealUuid);
		if (deal == null) {
			Deal deal1 = new Deal();
			deal1.setSecuredFunding(0.0);
			deal1.setUuid(dealUuid);
			deal1.setName(dealName);
			dealRepo.save(deal1);
		}
		investment.setAmount(amount);
		Deal dealObj = dealRepo.findByUuid(dealUuid);
		Integer uid = Integer.parseInt(userId);
		String userid = userId;
		System.out.println("userid -"+userid);
		User user = userRepo.findById(uid);
		String email = user.getEmail();
		System.out.println("email-"+email);
		investment.setUser(user);
		investment.setDeal(dealObj);
		investment.setEmail(email);
		investRepo.save(investment);
		Double dealAmount = dealRepo.findSecuredFundingById(dealUuid);
		dealRepo.updateSecuredFundingByUuid(dealAmount, dealUuid);
		return investment;

	}

	@SuppressWarnings("rawtypes")
	public String jsonParseObject(ResponseEntity response) {
		String jsonString = (String) response.getBody();
		System.out.println("jSonString - " + jsonString);
		// parsing data
		try {
			jObject = (JSONObject) (jParser.parse(jsonString));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		// String dUuid = (String) jObject.get("id");
		String dealName = (String) jObject.get("company");
		// System.out.println("duuid:" + dUuid);
		System.out.println("dealName-" + dealName);
		return dealName;
	}

	String contentDealObject = "";
	static JSONParser jParser = new JSONParser();
	static JSONObject jObject = null;

	@SuppressWarnings("rawtypes")
	public ResponseEntity callDealByIdOfContentService(String dealUuid) {
		System.out.println("dealUuid--" + dealUuid);
		String dealIdUrl = env.getProperty("dealIdUrl");
		String uri = dealIdUrl + dealUuid;
		System.out.println("uri-" + uri);
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate.getForEntity(uri, String.class);
		return response;
	}
}