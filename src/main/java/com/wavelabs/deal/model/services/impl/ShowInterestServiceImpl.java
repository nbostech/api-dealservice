package com.wavelabs.deal.model.services.impl;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.wavelabs.deal.domain.model.deal.Deal;
import com.wavelabs.deal.domain.model.user.User;
import com.wavelabs.deal.domain.repositories.DealRepository;
import com.wavelabs.deal.domain.repositories.InvestmentRepository;
import com.wavelabs.deal.domain.repositories.UserRepository;
import com.wavelabs.deal.model.service.ShowInterestService;
import com.wavelabs.deal.model.utils.DealDetailsOfContentService;

/**
 * 
 * @author thejasreem
 *
 */
@Service
public class ShowInterestServiceImpl implements ShowInterestService {
	static final Logger log = Logger.getLogger(ShowInterestServiceImpl.class);
	@Autowired
	InvestmentRepository investRepo;
	@Autowired
	DealRepository dealRepo;
	@Autowired
	UserRepository userRepo;
	@Autowired
	JavaMailSender sender;
	@Autowired
	Environment env;

	String contentDealObject = "";
	JSONParser jParser = new JSONParser();
	JSONObject jObject = null;

	/**
	 * /** showInterestOnDeal() method is for the people who shows interest on
	 * deal. if the given deal uuid doesn't exists, make a new entry in deal.
	 * 
	 * @return invest
	 */

	@Override
	public Deal showInterestOnDeal(String dealUuid, String userId) {
		@SuppressWarnings("rawtypes")
		ResponseEntity response = callDealByIdOfContentService(dealUuid);
		String dealName = DealDetailsOfContentService.jsonParseObject(response);
		// System.out.println("dealName-" + dealName);
		Deal deal1 = new Deal();

		Deal deal = dealRepo.findByUuid(dealUuid);
		if (deal == null) {
			deal1.setSecuredFunding(0.0);
			deal1.setUuid(dealUuid);
			deal1.setName(dealName);
			dealRepo.save(deal1);
		}
		String username = "";
		// System.out.println(userId);
		String uid = userId;
		// System.out.println(uid);
		Integer uId = Integer.parseInt(uid);
		// System.out.println(uId);
		User user = userRepo.findById(uId);
		// System.out.println(user);
		if (user != null) {
			username = user.getfirstname();
			// System.out.println("Name-" + username);
			// System.out.println("DealNAme - " + dealName);
			try {
				sendEmail(username, dealName);
			} catch (MessagingException e) {
				log.error(e);
			}
		}

		return deal1;

	}

	/**
	 * sendEmail() method sends an email to Admin about the people who are shown
	 * interest on a deal.
	 * 
	 * @param username
	 * @param dealname
	 * @throws MessagingException
	 */
	private void sendEmail(String username, String dealName) throws MessagingException {
		MimeMessage message = sender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message);
		helper.setTo("info@50kventures.com");
		helper.setText(username + " shown interest on a deal: " + dealName);
		helper.setSubject(username + " shown interest on a deal " + dealName + ".");
		sender.send(message);
	}

	ResponseEntity<String> response;

	@SuppressWarnings("rawtypes")
	public ResponseEntity callDealByIdOfContentService(String dealUuid) {
		// System.out.println("dealUuid--" + dealUuid);
		String uri = env.getProperty("dealIdUrl") + dealUuid;
		// String uri ="http://10.9.8.182:8033/deal/" + dealUuid;
		// System.out.println("uri-" + uri);
		RestTemplate restTemplate = new RestTemplate();
		try {
			response = restTemplate.getForEntity(uri, String.class);
		} catch (HttpClientErrorException e) {
			/*
			 * System.out.println(e.toString());
			 * System.out.println(e.getCause());
			 * System.out.println(e.getStackTrace());
			 * System.out.println(e.getRawStatusCode());
			 * System.out.println(e.getResponseBodyAsString());
			 * System.out.println(e.getRootCause());
			 * System.out.println(e.getMostSpecificCause());
			 * System.out.println(e.getMessage());
			 */
		}
		return response;
	}

}
