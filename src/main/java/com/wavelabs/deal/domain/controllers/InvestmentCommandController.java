package com.wavelabs.deal.domain.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.deal.domain.model.deal.Deal;
import com.wavelabs.deal.domain.model.investment.Investment;
import com.wavelabs.deal.domain.repositories.DealRepository;
import com.wavelabs.deal.domain.repositories.UserRepository;
import com.wavelabs.deal.model.event.service.DealUserEventsFactory;
import com.wavelabs.deal.model.event.service.DealUserEventsService;
import com.wavelabs.deal.model.events.DealUserEvents;
import com.wavelabs.deal.model.events.EventType;
import com.wavelabs.deal.model.service.InvestmentService;

import io.nbos.capi.api.v0.models.RestMessage;

/**
 * 
 * @author thejasreem InvestmentCommandController is the controller class for
 *         Investment class. createNewInvest() method is used for processing
 *         investment amount.
 * 
 */
@RestController
@Component
@RequestMapping(value = "/deal")
public class InvestmentCommandController {
	@Autowired
	InvestmentService investService;
	@Autowired
	DealUserEventsFactory dealUserEventsFactory;
	@Autowired
	DealUserEventsService eventService;
	@Autowired
	DealRepository dealRepo;
	@Autowired
	UserRepository userRepo;
	RestMessage restMessage = new RestMessage();

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/{userId}/investment/{dealUuid}/{amount}", method = RequestMethod.POST)
	public ResponseEntity processInvestedAmount(@PathVariable String userId, @PathVariable String dealUuid,
			@PathVariable double amount) {
		
		Investment value = investService.processInvestAmount(dealUuid, userId, amount);
		if (value != null ) {
			Deal deal = dealRepo.findByUuid(dealUuid);
			Integer dealId = deal.getId();
			//Integer uId = Integer.parseInt(userId);
			//User user = userRepo.findByEmail(userEmail);
			//int userId = user.getId();
			DealUserEvents event = dealUserEventsFactory.dealUserEventsForInvested(EventType.Invested,  dealId, userId);
			eventService.createEventsForDeals(event);
			restMessage.messageCode = "200";
			restMessage.message = "Investment  successfull!";
			return ResponseEntity.status(200).body(restMessage);
		} else {
			restMessage.messageCode = "404";
			restMessage.message = "Investment unsuccessfull!";
			return ResponseEntity.status(404).body(restMessage);
		}

	}
	

	}
