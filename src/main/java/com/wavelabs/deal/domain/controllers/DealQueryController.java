package com.wavelabs.deal.domain.controllers;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.wavelabs.deal.domain.model.deal.Deal;
import com.wavelabs.deal.domain.model.investment.Investment;
import com.wavelabs.deal.domain.repositories.DealRepository;
import com.wavelabs.deal.domain.repositories.DealUserEventRepository;
import com.wavelabs.deal.domain.repositories.InvestmentRepository;
import com.wavelabs.deal.domain.repositories.UserRepository;
import com.wavelabs.deal.model.event.service.DealUserEventsFactory;
import com.wavelabs.deal.model.event.service.DealUserEventsService;
import com.wavelabs.deal.model.events.DealUserEvents;
import com.wavelabs.deal.model.events.EventType;

import io.nbos.capi.api.v0.models.RestMessage;

/**
 * 
 * @author thejasreem DealQueryController is a class. getAllDeals() method
 *         retrieves all the deals from the uri(i.e from content service).
 * 
 */

@RestController
@Component
@RequestMapping(value = "/deal")
public class DealQueryController {
	static final Logger log = Logger.getLogger(DealQueryController.class);
	static final String PI = "peopleInterested";

	@Autowired
	Environment env;

	@Autowired
	DealRepository dealRepo;
	@Autowired
	InvestmentRepository investRepo;
	@Autowired
	DealUserEventsFactory dealUserEventsFactory;
	@Autowired
	DealUserEventsService eventService;
	@Autowired
	DealUserEventRepository dealEventRepo;
	@Autowired
	UserRepository userRepo;

	/*
	 @SuppressWarnings("rawtypes")
	 
	  @RequestMapping(method = RequestMethod.GET, value = "/all") public
	 ResponseEntity getAllDeals(@RequestHeader String userId) { RestTemplate restTemplate = new
	  RestTemplate(); RestMessage restMessage = new RestMessage(); String
	  dealUrl = env.getProperty("dealUrl"); ResponseEntity<String> response =
	 restTemplate.getForEntity(dealUrl, String.class); if (response != null) {
	  return ResponseEntity.status(200).body(response.getBody()); } else {
	 restMessage.message = "404"; restMessage.messageCode =
	  "Retrieval of Deals Fails!!"; return
	  ResponseEntity.status(404).body(restMessage); } }
	  */
	 
	JSONParser jParser = new JSONParser();
	JSONObject jObject = null;
	Investment[] investment = null;
	String dUuid = "";
	Integer dId = 0;

	/**
	 * getDealByUuid() method is for getting a particular deal using deal uuid.
	 * Bind the securedFunding, user deatils,amount to the content response
	 * object. if the given deal uuid doesn't exists in database return values
	 * as null ,otherwise add all existing values and bind it to response
	 * object.
	 * 
	 * @param dealUuid
	 * @return
	 * @throws ParseException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(method = RequestMethod.GET, value = "/{dealId}")
	public ResponseEntity getDealById(@PathVariable String dealId, @RequestHeader String userId) {
		String uri = env.getProperty("dealIdUrl") + dealId;
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate.getForEntity(uri, String.class);
		String jsonString = response.getBody();
		System.out.println(jsonString);

		// parsing data
		try {
			jObject = (JSONObject) (jParser.parse(jsonString));
			System.out.println(jObject);
			dUuid = (String) jObject.get("id");
			// querying the DB to chek if the UUID exists
			Deal dealObject = dealRepo.findByUuid(dUuid);
			// if the uuid exists
			if (dealObject != null) {
				Double amount = dealObject.getSecuredFunding();
				jObject.put("securedFunding", amount);
				investment = investRepo.findByDeal(dealObject);
				// creating a new JSONArray for peopleInterested
				JSONArray peopleInterested = new JSONArray();
				// setting user and amount as key value pairs to JSONOject.
				for (Investment in1 : investment) {
					JSONObject profile = new JSONObject();
					profile.put("user", in1.getUser());
					profile.put("amount", in1.getAmount());
					peopleInterested.add(profile);
				}
				if (peopleInterested.isEmpty()) {
					jObject.put(PI, null);
				} else {
					jObject.put(PI, peopleInterested);
				}
			} else {
				// if the uuid doesn't exist
				jObject.put(PI, null);
				jObject.put("securedFunding", 0.0);
			}
		} catch (NullPointerException ne) {
			log.error(ne);
		} catch (Exception e) {
			log.error(e);
		}
		Deal deal = dealRepo.findByUuid(dealId);
		if (deal != null) {
			dId = deal.getId();

		}
		// Integer uId = Integer.parseInt(userId);
		// User user = userRepo.findById(userId);
		DealUserEvents event = dealUserEventsFactory.dealUserEventsForViewed(EventType.Viewed, dId, userId);
		eventService.createEventsForDeals(event);
		return ResponseEntity.status(200).body(jObject);
	}

	/**
	 * Get all deals from content service using deal/all api. Get all the
	 * distinct viewed deals from deal_user_event table.(get all dId's) Store
	 * all did's in an array. Get all the dId's related Uuid's from the Deal
	 * table. Store duuid's in an array.
	 * 
	 * Check whether the deal object is in items, if yes check, dealObject uuid
	 * exists in the array of uuid's. if yes append isViewed field as true
	 * otherwise false.
	 * 
	 * @param userId
	 * @return
	 */
	ResponseEntity<String> response;
	JSONObject jObjectItems, jsonObject, jObjects;
	JSONArray jsonArray;
	@SuppressWarnings("rawtypes")
	Map map = new HashMap();

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(method = RequestMethod.GET, value = "/all")
	public ResponseEntity getAllDeals1(@RequestHeader String userId) throws ParseException {
		RestTemplate restTemplate = new RestTemplate();

		// System.out.println(userId);
		RestMessage restMessage = new RestMessage();
		JSONArray jObjectsinArray = new JSONArray();

		String dealUrl = env.getProperty("dealUrl");
		response = restTemplate.getForEntity(dealUrl, String.class);

		// System.out.println("response- " + response);
		String jObject = response.getBody();
		// System.out.println(jObject);

		jObjects = (JSONObject) (jParser.parse(jObject));
		jsonArray = (JSONArray) jObjects.get("items");
		System.out.println("array size - " + jsonArray);
		for (int i = 0; i < jsonArray.size(); i++) {
			jsonObject = (JSONObject) jsonArray.get(i);
			// System.out.println("jsonObject item --" + jsonObject);
			dUuid = (String) jsonObject.get("id");
			jObjects = checkDealInDB(dUuid);
			jObjectsinArray.add(jObjects);
		}

		map.put("total", jsonArray.size());
		map.put("totalPages", 1);
		map.put("pageSize", 10);
		map.put("currentPage", 1);
		map.put("items", jObjectsinArray);
		if (map != null) {

			return ResponseEntity.status(200).body(map);
		} else {
			restMessage.message = "Not found";
			restMessage.messageCode = "404";
			return ResponseEntity.status(404).body(restMessage);
		}

	}
	@SuppressWarnings("unchecked")
	public JSONObject checkDealInDB(String dUuid){
		// querying the DB to chek if the UUID exists
					Deal dealObject = dealRepo.findByUuid(dUuid);
					// if the uuid exists
					if (dealObject != null) {
						Double amount = dealObject.getSecuredFunding();
						jsonObject.put("securedFunding", amount);
						investment = investRepo.findByDeal(dealObject);
						// creating a new JSONArray for peopleInterested
						JSONArray peopleInterested = new JSONArray();
						// setting user and amount as key value pairs to JSONOject.
						for (Investment in1 : investment) {
							JSONObject profile = new JSONObject();
							profile.put("user", in1.getUser());
							profile.put("amount", in1.getAmount());
							peopleInterested.add(profile);
						}
						if (peopleInterested.isEmpty()) {
							jsonObject.put(PI, null);
						} else {
							jsonObject.put(PI, peopleInterested);
						}
					} else {
						// if the uuid doesn't exist
						jsonObject.put(PI, null);
						jsonObject.put("securedFunding", 0.0);
					}
					return jsonObject;
	}

}

/*
 * Integer id; String dUuid; Deal dealUuids; String dId; ArrayList<String>
 * dealuuids = new ArrayList<>(); int i = 0;
 * 
 * public JSONArray getDealId(Integer userId) { // get all dId's from event
 * table Integer[] dealIds = dealEventRepo.findAllById(userId); for (int i = 0;
 * i < dealIds.length; i++) { id = dealIds[i];
 * 
 * // get all deal Uuid's from deal table based on event table dId's dealUuids =
 * dealRepo.findAllById(id); dId = dealUuids.getUuid(); dealuuids.add(dId); }
 * 
 * JSONArray items = parseResponseOfContentService(response);
 * System.out.println(dealuuids.size()); return items; }
 * 
 * String dealUuid = ""; JSONParser parser = new JSONParser(); JSONObject obj;
 * JSONArray items; JSONObject obj2; JSONObject jObject;
 * 
 * @SuppressWarnings("unchecked") public JSONArray
 * parseResponseOfContentService(@SuppressWarnings("rawtypes") ResponseEntity
 * response) { String jsonString = (String) response.getBody(); try { obj =
 * (JSONObject) parser.parse(jsonString); System.out.println("Obj-" + obj);
 * items = (JSONArray) obj.get("items"); System.out.println("items-" + items);
 * for (int i = 0; i < items.size(); i++) { obj2 = (JSONObject) items.get(i);
 * System.out.println(obj2); dealUuid = (String) obj2.get("id"); // check deal
 * object exists in items if yes check // dealObject.uuid // exists in the array
 * of deal uuid's System.out.println("dealUuid-" + dealUuid);
 * 
 * Iterator<String> itr = dealuuids.iterator(); while (itr.hasNext()) { String
 * dealId = itr.next(); if (dealId.equals(dealUuid)) { obj2.put("isViewed",
 * true); System.out.println("sucess"); } else { obj2.put("isViewed", false);
 * System.out.println("fail"); } }
 * 
 * }
 * 
 * } catch (Exception e) { log.error(e); } return items; }
 */
