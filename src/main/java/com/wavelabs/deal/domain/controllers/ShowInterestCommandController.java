package com.wavelabs.deal.domain.controllers;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.deal.domain.model.deal.Deal;
import com.wavelabs.deal.domain.model.user.User;
import com.wavelabs.deal.domain.repositories.DealRepository;
import com.wavelabs.deal.domain.repositories.UserRepository;
import com.wavelabs.deal.model.event.service.DealUserEventsFactory;
import com.wavelabs.deal.model.event.service.DealUserEventsService;
import com.wavelabs.deal.model.events.DealUserEvents;
import com.wavelabs.deal.model.events.EventType;
import com.wavelabs.deal.model.service.ShowInterestService;

import io.nbos.capi.api.v0.models.RestMessage;

/**
 * 
 * @author thejasreem InvestmentCommandController is the controller class.
 *         showInterestOnDeal() method is for searching the people who has shown
 *         interest on a particular deal. If any one shows interest on a deal,
 *         admin receives an email.(User shows interest on a deal)
 */
@RestController
@Component
@RequestMapping(value = "/deal")
public class ShowInterestCommandController {
	static final Logger log = Logger.getLogger(ShowInterestCommandController.class);
	@Autowired
	ShowInterestService interestService;
	@Autowired
	DealRepository dealRepo;
	@Autowired
	UserRepository userRepo;
	@Autowired
	JavaMailSender sender;
	@Autowired
	DealUserEventsFactory dealUserEventsFactory;
	@Autowired
	DealUserEventsService eventService;
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/{dealUuid}/interest/{userId}", method = RequestMethod.POST)
	public ResponseEntity showInterestOnDeal(@PathVariable String dealUuid, @PathVariable String userId)
	{
		RestMessage restMessage = new RestMessage();
		Deal flag = interestService.showInterestOnDeal(dealUuid, userId);
		if (flag != null) {
			Deal deal = dealRepo.findByUuid(dealUuid);
			Integer dealId = deal.getId();
			Integer uId = Integer.parseInt(userId);
			User user = userRepo.findById(uId);
			Integer userid = user.getId();
			DealUserEvents event = dealUserEventsFactory.dealUserEventsForInterested(EventType.Interested, dealId, userid);
			eventService.createEventsForDeals(event);
			restMessage.messageCode = "200";
			restMessage.message = "User shown interest  successfull!";
			
			return ResponseEntity.status(200).body(restMessage);
		} else {
			restMessage.messageCode = "404";
			restMessage.message = "Investment unsuccessfull!";
			return ResponseEntity.status(404).body(restMessage);
		}

	}

	

}