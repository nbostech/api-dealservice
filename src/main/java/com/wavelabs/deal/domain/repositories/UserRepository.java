package com.wavelabs.deal.domain.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wavelabs.deal.domain.model.user.User;

/**
 * 
 * @author thejasreem
 * UserRepository is used for persistence.
 */

public interface UserRepository extends JpaRepository<User, Integer> {



	User findFirstNameByEmail(String userEmail);

User findById(Integer uid);

User findByEmail(Integer uid);




	
	

}
