package com.wavelabs.deal.domain.repositories;


import org.springframework.data.jpa.repository.JpaRepository;

import com.wavelabs.deal.domain.model.deal.Deal;
import com.wavelabs.deal.domain.model.investment.Investment;
import com.wavelabs.deal.domain.model.user.User;


public interface InvestmentRepository  extends JpaRepository<Investment, Integer> {

	public Investment[] findByDeal(Deal deal);


public User findByEmail(String userEmail);




public Investment[] findByDealIdAndEmail(Integer dId, String email);
	

}
