package com.wavelabs.deal.domain.repositories;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.wavelabs.deal.model.events.DealUserEvents;


/**
 * 
 * @author thejasreem DealUserEventRepository() for event persistence.
 */
public interface DealUserEventRepository extends CrudRepository<DealUserEvents, Integer> {

	
	@Query("select distinct e.deal.id from DealUserEvents e where e.user.id=:userId and e.eventType='Viewed'")
	Integer[] findAllById(@Param("userId")Integer userId);

}