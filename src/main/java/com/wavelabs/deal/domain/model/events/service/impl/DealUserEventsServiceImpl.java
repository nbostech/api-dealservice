package com.wavelabs.deal.domain.model.events.service.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wavelabs.deal.domain.repositories.DealUserEventRepository;
import com.wavelabs.deal.model.event.service.DealUserEventsService;
import com.wavelabs.deal.model.events.DealUserEvents;

/**
 * 
 * @author thejasreem DealUserEventsServiceImpl is the implementation class.
 *         DealUserEventsServiceImpl() method for storing events in database.
 * 
 */
@Service
public class DealUserEventsServiceImpl implements DealUserEventsService {
	static Logger log = Logger.getLogger(DealUserEventsServiceImpl.class);
	@Autowired
	DealUserEventRepository dealEventRepo;

	@Override
	public boolean createEventsForDeals(DealUserEvents dealUserEvents) {
		try {
			dealEventRepo.save(dealUserEvents);
			return true;
		} catch (Exception e) {
			log.error(e);
			return false;
		}
	}

}
