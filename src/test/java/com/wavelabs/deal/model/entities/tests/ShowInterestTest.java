/*package com.wavelabs.deal.model.entities.tests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.wavelabs.deal.domain.model.showinterest.ShowInterest;
import com.wavelabs.deal.model.utilities.ObjectBuilder;

@RunWith(MockitoJUnitRunner.class)

public class ShowInterestTest {
	@InjectMocks
	ShowInterest showInterestMock;
	ShowInterest showInterest;

	@Before
	public void setUp() {
		showInterest = ObjectBuilder.getShowInterest();
	}

	@Test
	public void testDefaultShowInterest() {
		showInterestMock.setDealUuid(showInterest.getDealUuid());
		Assert.assertEquals(showInterest.getDealUuid(), showInterestMock.getDealUuid());
		showInterestMock.setUserId(showInterest.getUserId());
		Assert.assertEquals(showInterest.getUserId(), showInterestMock.getUserId());
		showInterestMock.setUsername(showInterest.getUsername());
		Assert.assertEquals(showInterest.getUsername(), showInterestMock.getUsername());
		showInterestMock.setId(showInterest.getId());
		Assert.assertEquals(showInterest.getId(), showInterestMock.getId());
	}

	@Test
	public void testId() {
		showInterestMock.setDealUuid(showInterest.getDealUuid());
		Assert.assertEquals(showInterest.getDealUuid(), showInterestMock.getDealUuid());
	}

	@Test
	public void testUserUuid() {
		showInterestMock.setUserId(showInterest.getUserId());
		Assert.assertEquals(showInterest.getUserId(), showInterestMock.getUserId());
	}

	@Test
	public void testUsername() {
		showInterestMock.setUsername(showInterest.getUsername());
		Assert.assertEquals(showInterest.getUsername(), showInterestMock.getUsername());
	}

	@Test
	public void testDealUuid() {
		showInterestMock.setDealUuid(showInterest.getDealUuid());
		Assert.assertEquals(showInterest.getDealUuid(), showInterestMock.getDealUuid());
	}
}
*/