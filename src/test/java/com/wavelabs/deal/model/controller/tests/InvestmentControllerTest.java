/*package com.wavelabs.deal.model.controller.tests;

import static org.mockito.Matchers.anyDouble;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.http.ResponseEntity;

import com.wavelabs.deal.domain.controllers.InvestmentCommandController;
import com.wavelabs.deal.domain.model.investment.Investment;
import com.wavelabs.deal.domain.repositories.InvestmentRepository;
import com.wavelabs.deal.model.service.InvestmentService;
import com.wavelabs.deal.model.utilities.ObjectBuilder;

@RunWith(PowerMockRunner.class)
public class InvestmentControllerTest {
	@Mock
	InvestmentRepository investRepo;
	@Mock
	InvestmentService service;
	@InjectMocks
	InvestmentCommandController investmentController;

	@Test
	public void testInvestment() {
		Investment invest = ObjectBuilder.getInvestment();
		when(service.processInvestAmount(anyString(), anyString(), anyDouble())).thenReturn(invest);
		// Assert.assertEquals(200, entity.getStatusCodeValue());
	}

}
*/